import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ProductList } from '../Models/product-list.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
formData: ProductList;

  constructor(private firestore: AngularFirestore) { }

  createAddProduct(data) {
    return new Promise<any>((resolve, reject) => {
      this.firestore
        .collection('products')
        .add(data)
        .then(res => {}, err => reject(err));
    });
  }

  getProductList() {
    return this.firestore.collection('products', ref => ref.orderBy('productNumber', 'desc')).snapshotChanges();
  }

  updateProduct(data) {
    return this.firestore
      .collection('products')
      .doc(data.payload.doc.id),
      (this.formData = Object.assign({}, ));
  }

  deleteProduct(data) {
    return this.firestore
      .collection('products')
      .doc(data.payload.doc.id)
      .delete();
  }

  createProductOrder(data) {
    return new Promise<any>((resolve, reject) => {
      this.firestore
        .collection('productOrders')
        .add(data)
        .then(res => {}, err => reject(err));
    });
  }

  getOrderList() {
    return this.firestore.collection('productOrders', ref => ref.orderBy('orderNumber', 'desc')).snapshotChanges();
  }

  updateProductOrder(data) {
    return this.firestore
      .collection('productOrders')
      .doc(data.payload.doc.id)
      .set({ completed: true }, { merge: true });
  }

  deleteProductOrder(data) {
    return this.firestore
      .collection('productOrders')
      .doc(data.payload.doc.id)
      .delete();
  }
}
