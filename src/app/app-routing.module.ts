import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderListComponent } from './components/order/order-list/order-list.component';
import { OrdersComponent } from './components/order/orders/orders.component';
import { AddProductComponent } from './components/product/add-product/add-product.component';
import { ProductAppComponent } from './container/product-app/product-app.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: ProductAppComponent },
  { path: 'order-list', component: OrderListComponent },
  { path: 'order', component: OrdersComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
