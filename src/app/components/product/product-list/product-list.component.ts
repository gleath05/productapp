import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../shared/Services/order.service';
import { ProductList } from '../../../shared/Models/product-list.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: ProductList[];
  constructor(
    private orderService: OrderService,
    private firestore: AngularFirestore,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.getProducts();
  }


  getProducts() {
    this.orderService.getProductList().subscribe(actionArray => {
      this.products = actionArray.map(item => {
        return {
          id: item.payload.doc.id,
          ...item.payload.doc.data()
        } as ProductList;
      });
    });
  }
  // deleteProduct = data => this.orderService.deleteProduct(data);

  // onEdit = data => this.orderService.updateProduct(data);
  onEdit(product: ProductList) {
    this.orderService.formData = Object.assign({}, product);
  }

  onDelete(id: string) {
    if (confirm('Are you sure you want to delete?')) {
      this.firestore.doc('products/' + id).delete();
      this.toastr.warning('Deleted Successfully');
    }
    this.orderService.formData = {
      id: null,
      productName: '',
      productNumber: ''
    };
  }

}
