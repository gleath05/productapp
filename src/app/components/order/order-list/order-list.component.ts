import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../shared/Services/order.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {

  productOrders;
  constructor(private orderService: OrderService) { }

  ngOnInit() {
    this.getProductOrders();
  }

  getProductOrders = () =>
    this.orderService
      .getOrderList()
      .subscribe(res => (this.productOrders = res))

  deleteOrder = data => this.orderService.deleteProductOrder(data);

  markCompleted = data => this.orderService.updateProductOrder(data);

}
