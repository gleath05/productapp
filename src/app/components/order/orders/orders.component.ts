import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../shared/Services/order.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductList } from '../../../shared/Models/product-list.model';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  formOrder: FormGroup;
  list: ProductList[];
  constructor(
    private orderService: OrderService,
    private toastr: ToastrService,
    private fb: FormBuilder) { }

  productOrders = [];

  addProduct = product => this.productOrders.push(product);

  removeProduct = product => {
    const index = this.productOrders.indexOf(product);
    if (index > -1) { this.productOrders.splice(index, 1); }
  }
  ngOnInit() {
    this.formOrder = this.fb.group({
      customerName: ['', Validators.required],
      orderNumber: ['', [Validators.required, Validators.maxLength(5)]],
      productOrders: [''],
      completed: [false]
    });
    this.orderService.getProductList().subscribe(actionArray => {
      this.list = actionArray.map(item => {
        return {
          id: item.payload.doc.id,
          ...item.payload.doc.data()
        } as ProductList;
      });
    });
}

  onSubmit() {

    if (this.productOrders.length === 0) {
      this.toastr.warning('No product selected!');

    } else {
      this.formOrder.value.productOrders = this.productOrders;
      const data = this.formOrder.value;

      console.log('success', data);
      this.orderService.createProductOrder(data).then(res => {
        /*do something here....maybe clear the form or give a success message*/
      });
      this.toastr.success('Submit order success!');
      this.formOrder.reset();
      this.productOrders = [];
    }
  }
}
