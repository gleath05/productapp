import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OrderListComponent } from './components/order/order-list/order-list.component';
import { OrdersComponent } from './components/order/orders/orders.component';
import { OrderService } from './shared/Services/order.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AddProductComponent } from './components/product/add-product/add-product.component';
import { ProductListComponent } from './components/product/product-list/product-list.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProductAppComponent } from './container/product-app/product-app.component';

@NgModule({
  declarations: [
    AppComponent,
    OrderListComponent,
    OrdersComponent,
    AddProductComponent,
    ProductListComponent,
    ProductAppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    BrowserAnimationsModule,
    NgxPaginationModule,
    ToastrModule.forRoot({ preventDuplicates: true })
  ],
  providers: [OrderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
